json.array!(@jobs) do |job|
  json.extract! job, :id, :job_name, :employee_id, :job_complete, :job_location, :due_date
  json.url job_url(job, format: :json)
end
