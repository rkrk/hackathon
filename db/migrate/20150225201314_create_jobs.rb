class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :job_name
      t.integer :employee_id
      t.boolean :job_complete
      t.string :job_location
      t.date :due_date

      t.timestamps null: false
    end
  end
end
