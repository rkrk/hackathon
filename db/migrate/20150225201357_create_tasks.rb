class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :task_name
      t.boolean :task_complete
      t.string :task_description
      t.integer :job_id

      t.timestamps null: false
    end
  end
end
